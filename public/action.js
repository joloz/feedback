$(".title span")
  .on("mouseenter", function () {
    $(this).next().css("font-weight", "300").css("opacity", "0.75");
    $(this).prev().css("font-weight", "300").css("opacity", "0.75");
    $(this).css("font-weight", "600").css("opacity", "1");
  })
  .on("mouseleave", function () {
    $(this).next().css("font-weight", "1").css("opacity", "0.5");
    $(this).prev().css("font-weight", "1").css("opacity", "0.5");
    $(this).css("font-weight", "1").css("opacity", "0.5");
  });
